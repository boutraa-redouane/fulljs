var express = require('express');
var hostname = 'localhost';
var port = 3000;
var mongoose = require('mongoose');

var options = {
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }
};

var urlmongo = "mongodb://localhost:27017/prod";
mongoose.connect(urlmongo, options);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function () {
    console.log("Connexion à la base OK");
});

var panierSchema = mongoose.Schema({
    id: String,
    nom: String,
    description: String,
    legume: {
        a: String,
        b: String,
        c: String
    }
});
var Panier = mongoose.model('Panier', panierSchema);

var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



var myRouter = express.Router();

myRouter.route('/')
    .all(function (req, res) {
        res.json({ message: "Bienvenue sur notre Panier API ", methode: req.method });
    });

myRouter.route('/paniers')
.get(function (req, res) {
        Panier.find((err, paniers) => {
            if (err) {
                res.send(err);
            }
            res.json(paniers);
        });
    })
    .post(function (req, res) {
        var panier = new Panier();
        panier.nom = req.body.nom;
        panier.description = req.body.description;
        panier.legume = req.body.legume;
        panier.save(function (err) {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Bravo, la panier est maintenant stockée en base de données' });
        });
    });

myRouter.route('/paniers/:panier_id')
    .get((req, res) => {
        Panier.findById(req.params.panier_id, (err, panier) => {
            if (err)
                res.send(err);
            res.json(panier);
        });
    })
    .put((req, res) => {
        Panier.findById(req.params.panier_id, (err, panier) => {
            if (err) {
                res.send(err);
            }
            panier.nom = req.body.nom;
            panier.description = req.body.description;
            panier.save(function (err) {
                if (err) {
                    res.send(err);
                }
                res.json({ message: 'Bravo, mise à jour des données OK' });
            });
        });
    })
    .delete(function (req, res) {

        Panier.remove({ _id: req.params.panier_id }, function (err, panier) {
            if (err) {
                res.send(err);
            }
            res.json({ message: "Bravo, panier supprimée" });
        });

    });
app.use(myRouter);


app.listen(port, hostname, function () {
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});