var app = require('./app.js')

let port = 3000;
let hostname = 'localhost';

app.listen(port, hostname, () => {
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});