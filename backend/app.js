const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const panierRoutes = require('./routes/panier');
const userRoutes = require('./routes/user');

const app = express();

//MongoDb connexion
var options = {
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }
};

var urlmongo = "mongodb://localhost:27017/prod";
mongoose.connect(urlmongo, options);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function () {
    console.log("Connexion à la base OK");
});

//App parametrage
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/paniers', panierRoutes);
app.use('/api/auth', userRoutes);


module.exports = app;