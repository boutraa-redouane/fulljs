const Panier = require('../models/panier');

exports.getAllPanier = (req, res, next) => {
    Panier.find().then(
        (paniers) => {
            res.status(200).json(paniers);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.createPanier = (req, res, next) => {
    var panier = new Panier(req.body);
    panier.save().then( () => {
        res.status(201).json({message: "panier ajouté"});
        }
    ).catch((err) => { 
        res.status(400).json({
            error: erreur
        });
        }
    );
};

exports.getOnePanier = (req, res, next) => {
    Panier.findOne({_id: req.params.id}).then((panier) => {
        res.status(200).json(panier);
    }).catch(
        (err) => {
            res.status(404).json({error: "not found"});
        }
    );
};

exports.modifyPanier = (req, res, next) => {
    console.log("modifie");
    const panier = new Panier({
        _id: req.params.id,
        nom: req.body.nom,
        description: req.body.description,
        prix: req.body.prix
    });
    Panier.updateOne({ _id: req.params.id}, panier).then(
        () => {
            res.status(201).json({message: 'panier modifié'});
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

exports.deletePanier = (req, res, next) => {
    Panier.deleteOne({ _id: req.params.id }).then(
        () => { 
            res.status(200).json({message: "panier supprimé"});
        }
    ).catch(
        (err) => {
            res.status(400).json({error: erreur});
        }
    );
};
