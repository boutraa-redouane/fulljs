const mongoose = require('mongoose');

var panierSchema = mongoose.Schema({
    id: String,
    nom: String,
    description: String,
    prix: Number,
    legume: {
        a: String,
        b: String,
        c: String
    }
});

module.exports = mongoose.model('Panier', panierSchema);