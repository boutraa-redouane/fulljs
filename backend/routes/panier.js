const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const panierCtrl = require('../controllers/panier');

router.get('/', panierCtrl.getAllPanier);
router.post('/', auth, panierCtrl.createPanier);
router.get('/:id', panierCtrl.getOnePanier);
router.put('/:id', auth, panierCtrl.modifyPanier);
router.delete('/:id', auth, panierCtrl.deletePanier);

module.exports = router;